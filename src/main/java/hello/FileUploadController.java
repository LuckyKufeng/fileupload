package hello;

import java.io.*;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class FileUploadController {

    @RequestMapping(value="/upload", method=RequestMethod.GET)//This line means a value retrieved from the url path (/upload) and not from a request parameter.
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam("name") String name,
                                                 @RequestParam("file") MultipartFile file){
        String returnText = "You successfully uploaded " + name + "!";//this return text is the message what I want to return to the page.
        if (!file.isEmpty()) {  // if the file we choose is not empty.
            BufferedOutputStream stream = null;
            try {
                byte[] bytes = file.getBytes(); // we translate the file with getBytes() method and store this file into a byte array.
                stream = new BufferedOutputStream(new FileOutputStream(new File("upload/" + name)));// create an output stream and give the path we want to store that file.
                stream.write(bytes);//writes b.length bytes from the specified byte array to this output stream.
            } catch (Exception e) {
                returnText =  "You failed to upload " + name + " => " + e.getMessage();// if there is an exception, user will see the error message creating by e.getMessage()
            }finally{// Create finally block to close the stream.
                try{
                    if(stream != null) {// this line of code is used to avoid null pointer exception.
                        stream.close();// close this output stream
                    }
                }catch (Exception e){
                    returnText = e.getMessage();
                }
            }
        } else {
            returnText =  "You failed to upload " + name + " because the file was empty.";// if the file is empty, we will see this message on the web page.
        }
        return returnText;// Here is only one return statement in the method
    }


    @RequestMapping(value = "/download", method = RequestMethod.GET)// This line means a value retrieved from the url path and not from a request parameter.
    @ResponseBody
    public FileSystemResource getFile() {
        File file = new File("upload/123.txt"); // I initialize the download document.
        if (!file.canRead()) { //if the file can't be read. I will return null to the page.
            return null;
        }
        return new FileSystemResource(file);// if the file can be read. I will create a fileSystemResource.return this fileSystemResource to the page. We could see the content of that file on the page.
    }


}