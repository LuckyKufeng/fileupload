What is it?
=============================
This is a simple program which we could use to upload and download files to/from a web server.


Methods
=============================
There are two main parts of that Java controller.

1. upload controller
-----------------------------
 public @ResponseBody String handleFileUpload(@RequestParam("name") String name, 
            @RequestParam("file") MultipartFile file){
	....
	}

Here we need 2 parameters.
1) @RequestParam("name") String name
We will get "name" from the index.html page with this line :<input type="text" name="name">
This text which we type in will be stored as the name of that document in the server.
In this field, we need a full document type, such as 123.txt , myFaver.doc.

2) @RequestParam("file") MultipartFile file
We will get the "file" from the index.html page with this line: <input type="file" name="file"><br />
We need to choose a file in our computer.

In the index.html page, we have a form.
<input type="submit" value="Upload"> Press here to upload the file!
After we click the submit button, we will send these two parameters with a POST method.


BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("upload/" + name)));
This line of code will create an output stream and give the path we want to store that file with the name.-->new File("upload/" + name)
stream.write(bytes);//writes b.length bytes from the specified byte array to this output stream.
stream.close();// close this output stream

2. download controller
---------------------------------
 public FileSystemResource getFile() {
	...
	}

@RequestMapping(value = "/download", method = RequestMethod.GET)
This line means a value retrieved from the url path and not from a request parameter.
I also add a link in the index.html page.
<a href="http://localhost:8080/download?name=123.txt">Click this link to download</a>
By clicking this link, we will download the file with the name = 123.txt.
And we could see the content of this file in a new page.


Others
========================================
multipart.maxFileSize: 128KB
It means total file size we upload cannot exceed 128KB.
multipart.maxRequestSize: 128KB
It means total request size for a multipart/form-data cannot exceed 128KB



